# Movie Booking System
It is a system that helps the user to book a movie ticket for desired Showtime and at desired Cinema.

### Development Tools
- **Programming Language:** Java
- **Integrated Development Environment:** Eclipse
- * JDK 1.8.
- * Junit 4.12

## Constraints
1.   Movie Booking System has multiple cinemas and each cinema has multiple movie shows for different movies.
2.   The limit of seats for each show is 100. So, if user enters more than 100 seats an message would be displayed stating booking failed.
3.   After every booking the count of unoccupied seats should decrease for a given movie show.




## How to Run
1. Run the program UserInterface. This is the driver program.
    *  yes

2.  The user enters number of cinemas one wishes to create. Validation of number of cinemas entered by user is done by Service Class. The number must be greater tha zero and less than 10.
3.  User is asked to enter the name of these cinemas.
4.  Using certain algorithm showtimes are randomly created.
5.  A list of all the movie show with show number , timings , cinema name and movie name is displayed.
6.  User is asked to select the cinema , show number , movie and number of tickets to be booked.
7.  The Status of the ticket booking is shown.