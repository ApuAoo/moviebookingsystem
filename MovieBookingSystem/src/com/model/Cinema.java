package com.model;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Random;

public class Cinema {
	public ArrayList<Shows> shows;
	String cinemaName;
	public Cinema( String cinemaName) {
		super();
		this.shows = new ArrayList<>();
		this.cinemaName = cinemaName;
	}
	public ArrayList<Shows> getShows() {
		return shows;
	}
	public void setShows(ArrayList<Shows> shows) {
		this.shows = shows;
	}
	public String getCinemaName() {
		return cinemaName;
	}
	public void setCinemaName(String cinemaName) {
		this.cinemaName = cinemaName;
	}
public void createShows(int index,String movieName,Time timeCinema ) {
     this.shows.add(index, new Shows(movieName, timeCinema));	
}
public void displayShows()
{
	for(int i=0;i<shows.size();i++)
	{
		System.out.println(getCinemaName() +" " +"show no"+ i +shows.get(i).getMovie() +"  " + shows.get(i).getTime());
	}
	}
}
