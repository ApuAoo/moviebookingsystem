package com.model;

import java.sql.Time;

public class Shows extends SeatCapacity {
	String movie;
	Time time;
	public Shows(String movie, Time time) {
		super();
		this.movie = movie;
		this.time = time;
	}
	public String getMovie() {
		return movie;
	}
	public void setMovie(String movie) {
		this.movie = movie;
	}
	public Time getTime() {
		return time;
	}
	public void setTime(Time time) {
		this.time = time;
	}

}
