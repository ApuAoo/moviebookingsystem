package com.Service;

import com.dao.MovieBookingDAOImpl;
import com.exception.numberOfCinemaExceptions;
import com.exception.wrongMovieShowSelectionException;
import com.exception.wrongSelectionOfCinemaException;

public class MovieBookingServiceValidations {
	public boolean isNumberOfCinemaValid(int number) {
		if(number<0)
		{
			try {
				throw new numberOfCinemaExceptions("Enter a Valid number of Cinema");
				
			} catch (numberOfCinemaExceptions e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return false;
		}
		return true;
	}
	public boolean isValidCinemaName(MovieBookingDAOImpl daoImpl,String cinemaName, String movieName, int showNumber)
	{
		for(int i=0;i<daoImpl.cinemas.size();i++)
		{
			if(daoImpl.cinemas.get(i).getCinemaName().equalsIgnoreCase(cinemaName))
			{
				for(int j=0;j<daoImpl.cinemas.get(i).getShows().size();j++)
				{
		            if(daoImpl.cinemas.get(i).getShows().get(j).getMovie().equalsIgnoreCase(movieName)&&j==showNumber )
		            {
					
		            	return true;
			}
			}
			}
			
		}
		try {
			throw new wrongSelectionOfCinemaException("you have entered somthing invalid");
		} catch (wrongSelectionOfCinemaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return false;
	}
	
	}

