package com.Service;

import com.dao.MovieBookingDAOImpl;

public class MovieBookingServiceImpl implements MovieBookingService {
	 MovieBookingServiceValidations validations;
	 MovieBookingDAOImpl daoImpl;

	@Override
	public boolean creatingCinemas(int numberOfCinemas) {
	 this.validations =new MovieBookingServiceValidations();
	
	return validations.isNumberOfCinemaValid(numberOfCinemas);
	}

	@Override
	public void setCinema(int i ,String cinemaName) {

   
    daoImpl.setCinema(i,cinemaName);
    return;
     
		
	}
	public MovieBookingServiceImpl() {
	this.daoImpl=new MovieBookingDAOImpl();
	}

	public void showCinema()
	{
		
	    daoImpl.showCinema();
	}
 public void createShows(String movieName,int numberOfShows)
 {
	daoImpl.createShows(movieName,numberOfShows);
 }

@Override
public void displayShows() {

daoImpl.displayShows();
	
}

@Override
public void bookShow(String cinemaName, String movieName, int showNumber,int tickets) {
	if(validations.isValidCinemaName(this.daoImpl,cinemaName, movieName, showNumber))
	{
		daoImpl.bookShow(cinemaName, movieName, showNumber,tickets);
	}
	
}
	
	
}
