package com.Service;

import com.dao.MovieBookingDAOImpl;

public interface MovieBookingService {
public void setCinema(int i,String cinemaName);
public boolean	creatingCinemas(int numberOfCinemas);
public void showCinema();
public void displayShows();
public void bookShow(String cinemaName,String movieName,int showNumber ,int ticket);


}
