package com.dao;

import com.model.Cinema;

public interface MovieBookingDAO {

	public void setCinema(int i,String cinemaName);
	public void showCinema();
	public void createShows(String movieName, int numberOfShows) ;
	public void displayShows();
	public void bookShow(String cinemaName, String movieName, int showNumber,int tickets) ;
}
