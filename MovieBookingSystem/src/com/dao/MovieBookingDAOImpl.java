package com.dao;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Random;

import com.model.Cinema;

public class MovieBookingDAOImpl implements MovieBookingDAO {
	public ArrayList<Cinema> cinemas;
	
	public MovieBookingDAOImpl() {
	
		this.cinemas = new ArrayList<>();
	}

	@Override
	public void setCinema(int i ,String cinemaName) {
		
		this.cinemas.add(i, new Cinema(cinemaName));
		return;
	}

	@Override
	public void showCinema() {
	for(int i=0;i<this.cinemas.size();i++)
	{
		System.out.println(i+ "  "+this.cinemas.get(i).getCinemaName());
	}
	}

	@Override
	public void createShows(String movieName, int numberOfShows) {
		for (int i = 0; i < cinemas.size(); i++) {
			for (int j = 0; j <numberOfShows; j++) {
				final Random random = new Random();
				final int millisInDay = 24*60*60*1000;
				Time time = new Time((long)random.nextInt(millisInDay));
			cinemas.get(i).createShows(j,movieName,time);
			}	 			
		}
		
	}

	@Override
	public void displayShows() {
		for(int i=0;i<cinemas.size();i++)
		{
			cinemas.get(i).displayShows();
		}
		
	}

	@Override
	public void bookShow(String cinemaName, String movieName, int showNumber ,int tickets) {
		for(int i=0;i<cinemas.size();i++)
		{
			if(cinemas.get(i).getCinemaName().equalsIgnoreCase(cinemaName))
			{
				for(int j=0;j<cinemas.get(i).getShows().size();j++)
				{
		            if(cinemas.get(i).getShows().get(j).getMovie().equalsIgnoreCase(movieName)&&j==showNumber )
		            {
		            	if(cinemas.get(i).getShows().get(showNumber).bookTicket(tickets))
		            	{
		            		System.out.println("Tickets has been booked");
		            		return;
		            	}
		            }
					
		
		
	}
	
	

	
}
		}
		}
}
